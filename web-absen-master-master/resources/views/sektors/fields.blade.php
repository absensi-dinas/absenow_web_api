<!-- Nama Field -->
<div class="col-sm-12">
    {!! Form::label('nama', 'Sektor:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<div class="col-sm-12">
    {!! Form::label('latitude', 'Latitude :') !!}
    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
</div>

<div class="col-sm-12">
    {!! Form::label('longtitude', 'Longitude:') !!}
    {!! Form::text('longtitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('sektors.index') }}" class="btn btn-default">Cancel</a>
</div>
